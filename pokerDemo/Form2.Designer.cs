﻿namespace poker
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.imieTxt = new System.Windows.Forms.TextBox();
            this.pieniadzeTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Imie";
            // 
            // imieTxt
            // 
            this.imieTxt.Location = new System.Drawing.Point(147, 41);
            this.imieTxt.Name = "imieTxt";
            this.imieTxt.Size = new System.Drawing.Size(100, 20);
            this.imieTxt.TabIndex = 1;
            // 
            // pieniadzeTxt
            // 
            this.pieniadzeTxt.Location = new System.Drawing.Point(139, 86);
            this.pieniadzeTxt.Name = "pieniadzeTxt";
            this.pieniadzeTxt.Size = new System.Drawing.Size(100, 20);
            this.pieniadzeTxt.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ilość pieniędzy";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(100, 167);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pieniadzeTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.imieTxt);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Text = "Gracz1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox imieTxt;
        private System.Windows.Forms.TextBox pieniadzeTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}