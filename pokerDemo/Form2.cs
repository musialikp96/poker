﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace poker
{
    public partial class Form2 : Form
    {
        Form1 form1 = new Form1();
        Form1 parent;
        int ile = 0;
        public Form2(Form1 form)
        {
            InitializeComponent();
            parent = form;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            switch (ile)
            {
                case 0:
                    {
                        parent.gracz1.imie = imieTxt.Text;
                        parent.gracz1.ilePieniedzy = double.Parse(pieniadzeTxt.Text);
                    }
                    break;
                case 1:
                    {
                        parent.gracz2.imie = imieTxt.Text;
                        parent.gracz2.ilePieniedzy = double.Parse(pieniadzeTxt.Text);
                    }
                    break;
            }
            

            this.Text = "Gracz2";
            imieTxt.Text = "";
            pieniadzeTxt.Text = "";
            if (ile == 1)
            {
                this.Close();
            }
            
            ile++;
        }
    }
}
