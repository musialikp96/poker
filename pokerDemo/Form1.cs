﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace poker
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        public gracz gracz1 = new gracz();
        public gracz gracz2 = new gracz();
        public int ilePrzydzielonych = 0;
        public int[] talia = new int[24];
        bool[] klikniete = new bool[5];
        int biezace = 2;
        int ileKliknietych = 0;
        int ileKlikniecBut3 = 0;
        int pieniadzePuli = 0;
        int ileDoPuli = 0;

        public Form1()
        {
            InitializeComponent();
        }
        private int wylosuj()
        {
            return rnd.Next(1, 24);
        }
        private void pula(int pula, double pieniadze)
        {
            pieniadze += pula;
            pula = 0;
        }
        private void remis()
        {
            string remis = "REMIS - otrzymujecie po 1 punkcie, a pula przechodzi do kolejnej gry.";
            MessageBox.Show(remis);
            gracz1.iloscPunktow++;
            gracz2.iloscPunktow++;
        }
        private void wygrana(gracz gracz, string uklad)
        {
            string wygrana = "Wygrywa " + gracz.imie + " " + uklad + " \nBrawo " + gracz.imie + "! Otrzymujesz 2 pkty i kase z puli";
            MessageBox.Show(wygrana);
            gracz.iloscPunktow += 2;
            pula(pieniadzePuli, gracz.ilePieniedzy);
        }

        private void sedzia()
        {
            if (uklady.pokerKrolewski(gracz1) == true)
            {
                if (uklady.pokerKrolewski(gracz2) == false)
                {
                    wygrana(gracz1, "POKER KROLEWSKI");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.pokerKrolewski(gracz2) == true)
            {
                if (uklady.pokerKrolewski(gracz1) == false)
                {
                    wygrana(gracz2, "POKER KROLEWSKI");
                }
                else
                {
                    remis();
                }
            }


            else if (uklady.poker(gracz1) == true)
            {
                if (uklady.poker(gracz2) == false)
                {
                    wygrana(gracz1, "POKER");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.poker(gracz2) == true)
            {
                if (uklady.poker(gracz1) == false)
                {
                    wygrana(gracz2, "POKER");
                }
                else
                {
                    remis();
                }
            }

            else if (uklady.kareta(gracz1) == true)
            {
                if (uklady.kareta(gracz2) == false)
                {
                    wygrana(gracz1, "KARETA");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.kareta(gracz2) == true)
            {
                if (uklady.kareta(gracz1) == false)
                {
                    wygrana(gracz2, "KARETA");
                }
                else
                {
                    remis();
                }
            }

            else if (uklady.full(gracz1) == true)
            {
                if (uklady.full(gracz2) == false)
                {
                    wygrana(gracz1, "FULL");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.full(gracz2) == true)
            {
                if (uklady.full(gracz1) == false)
                {
                    wygrana(gracz2, "FULL");
                }
                else
                {
                    remis();
                }
            }

            else if (uklady.kolor(gracz1) == true)
            {
                if (uklady.kolor(gracz2) == false)
                {
                    wygrana(gracz1, "KOLOR");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.kolor(gracz2) == true)
            {
                if (uklady.kolor(gracz1) == false)
                {
                    wygrana(gracz2, "KOLOR");
                }
                else
                {
                    remis();
                }
            }

            else if (uklady.strit(gracz1) == true)
            {
                if (uklady.strit(gracz2) == false)
                {
                    wygrana(gracz1, "STRIT");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.strit(gracz2) == true)
            {
                if (uklady.strit(gracz1) == false)
                {
                    wygrana(gracz2, "STRIT");
                }
                else
                {
                    remis();
                }
            }

            else if (uklady.trojka(gracz1) == true)
            {
                if (uklady.trojka(gracz2) == false)
                {
                    wygrana(gracz1, "TROJKA");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.trojka(gracz2) == true)
            {
                if (uklady.trojka(gracz1) == false)
                {
                    wygrana(gracz2, "TROJKA");
                }
                else
                {
                    remis();
                }
            }

            else if (uklady.dwiePary(gracz1) == true)
            {
                if (uklady.dwiePary(gracz2) == false)
                {
                    wygrana(gracz1, "2 PARY");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.dwiePary(gracz2) == true)
            {
                if (uklady.dwiePary(gracz1) == false)
                {
                    wygrana(gracz2, "2 PARY");
                }
                else
                {
                    remis();
                }
            }

            else if (uklady.para(gracz1) == true)
            {
                if (uklady.para(gracz2) == false)
                {
                    wygrana(gracz1, "PARA");
                }
                else
                {
                    remis();
                }
            }
            else if (uklady.para(gracz2) == true)
            {
                if (uklady.para(gracz1) == false)
                {
                    wygrana(gracz2, "PARA");
                }
                else
                {
                    remis();
                }
            }
        }

        private void mieszaj()
        {
            for (int j = 0; j < 100; j++)
                for (int i = 0; i < talia.Count(); i++)
                {
                    int los = wylosuj();
                    int pom;
                    pom = talia[i];
                    talia[i] = talia[los];
                    talia[los] = pom;
                }
        }
        private void sortuj(gracz Gracz)
        {
            Array.Sort(Gracz.karty);
        }
        private void przydziel(gracz Gracz)
        {
            for (int i = 0; i < 5; i++)
            {
                Gracz.karty[i] = talia[i + ilePrzydzielonych];
            }
            ilePrzydzielonych += 5;
            sortuj(Gracz);
        }

        private void przelacz()
        {
            string sciezka= Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory())) +@"\karty\";
            if (biezace == 1)
            {
                numericUpDown1.Value = 0;
                label1.Text = "Gracz: " + gracz2.imie;
                label2.Text = "Pieniądze: "+gracz2.ilePieniedzy;
                numericUpDown1.Maximum = decimal.Parse(gracz2.ilePieniedzy.ToString());
                pictureBox15.ImageLocation = sciezka + gracz2.karty[0] + ".png";
                pictureBox14.ImageLocation = sciezka + gracz2.karty[1] + ".png";
                pictureBox13.ImageLocation = sciezka + gracz2.karty[2] + ".png";
                pictureBox12.ImageLocation = sciezka + gracz2.karty[3] + ".png";
                pictureBox11.ImageLocation = sciezka + gracz2.karty[4] + ".png";
                biezace = 2;
            }
            else if (biezace == 2)
            {
                numericUpDown1.Value = 0;
                label1.Text = "Gracz: " + gracz1.imie;
                label2.Text = "Pieniądze: " + gracz1.ilePieniedzy;
                numericUpDown1.Maximum = decimal.Parse(gracz1.ilePieniedzy.ToString());
                pictureBox15.ImageLocation = sciezka + gracz1.karty[0] + ".png";
                pictureBox14.ImageLocation = sciezka + gracz1.karty[1] + ".png";
                pictureBox13.ImageLocation = sciezka + gracz1.karty[2] + ".png";
                pictureBox12.ImageLocation = sciezka + gracz1.karty[3] + ".png";
                pictureBox11.ImageLocation = sciezka + gracz1.karty[4] + ".png";
                biezace = 1;
            }
        }//zmiana aktualnych kart na karty innego gracza
        private void wymienKarty(gracz Gracz)
        {
            int indexTalii = ilePrzydzielonych - 1;
            for (int i = 0; i < ileKliknietych; i++)
            {
                int a = Gracz.karty[Gracz.doWymiany[i]];
                Gracz.karty[Gracz.doWymiany[i]] = talia[indexTalii++];
                ilePrzydzielonych++;
            }
            przelacz();
            przelacz();
            Gracz.wymienil = true;
        }
        private void przelaczeniePoWymianie()
        {
            przelacz();
        }
        private void wynik()
        {

        }
        private void zerujZmienionaLokacjeKart()
        {
            pictureBox15.Location = new Point(pictureBox15.Location.X, 346);
            pictureBox14.Location = new Point(pictureBox14.Location.X, 346);
            pictureBox13.Location = new Point(pictureBox13.Location.X, 346);
            pictureBox12.Location = new Point(pictureBox12.Location.X, 346);
            pictureBox11.Location = new Point(pictureBox11.Location.X, 346);
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (gracz1.wymienil!=true && gracz2.wymienil!=true)
            {
                int ile = sprawdzKlikniete();

                switch (biezace)
                {
                    case 1:
                        {
                            if (gracz1.wymienil == false)
                            {

                                wymienKarty(gracz1);
                                //wyswietlKarty(gracz1);
                            }
                            else MessageBox.Show("WYMIENIONO");
                        }
                        break;
                    case 2:
                        {
                            if (gracz2.wymienil == false)
                            {
                                wymienKarty(gracz2);
                                //wyswietlKarty(gracz2);
                            }
                            else MessageBox.Show("WYMIENIONO");
                        }
                        break;
                }
                zerujZmienionaLokacjeKart();
                for (int i = 0; i < 5; i++)
                    klikniete[i] = false;
                ileKliknietych = 0;

                przelaczeniePoWymianie();
            }
            else
            {
                sedzia();
                zerujZmienionaLokacjeKart();
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            mieszaj();
            przydziel(gracz1);
            przydziel(gracz2);
            //wyswietlKarty(gracz1);
            // wyswietlKarty(gracz2);
        }

        public void zmienPolozenie(PictureBox box, int indexKlikniete)
        {
            int Y = int.Parse(box.Location.Y.ToString());
            int X = int.Parse(box.Location.X.ToString());
            int index = indexKlikniete;
            if (klikniete[indexKlikniete] == false && ileKliknietych < 3)
            {
                box.Location = new System.Drawing.Point(X, Y - 50);
                klikniete[indexKlikniete] = true;
                ileKliknietych++;
            }
            else if (klikniete[indexKlikniete] == true)
            {
                box.Location = new System.Drawing.Point(X, Y + 50);
                klikniete[indexKlikniete] = false;
                ileKliknietych--;
            }
        }

        private int sprawdzKlikniete()
        {
            int j = 0;
            for (int i = 0; i < 5; i++)
            {
                gracz1.doWymiany[i] = 0;
            }

            for (int i = 0; i < 5; i++)
            {
                if (klikniete[i] == true)
                {
                    gracz1.doWymiany[j++] = i;
                }
            }
            return j;
        }
        #region KARTY

        private void pictureBox15_Click(object sender, EventArgs e)
        {
            zmienPolozenie(pictureBox15, 0);
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            zmienPolozenie(pictureBox14, 1);
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            zmienPolozenie(pictureBox13, 2);
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            zmienPolozenie(pictureBox12, 3);
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            zmienPolozenie(pictureBox11, 4);
        }
        //USTAWIANIE KURSORA:
        private void pictureBox15_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void pictureBox15_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void pictureBox14_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void pictureBox14_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }


        private void pictureBox13_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void pictureBox13_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void pictureBox12_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void pictureBox12_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void pictureBox11_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void pictureBox11_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public void rozpocznij()
        {
            gracz1.wymienil = false;
            gracz2.wymienil = false;
            for (int i = 0; i < talia.Count(); i++)
                talia[i] = i + 1;

            for (int i = 0; i < klikniete.Count(); i++)
                klikniete[i] = false;

            mieszaj();
            przydziel(gracz1);
            przydziel(gracz2);
            //wyswietlKarty(gracz1);
            //wyswietlKarty(gracz2);
            przelacz();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Form2 nowyGracz = new Form2(this);
            nowyGracz.ShowDialog();
            rozpocznij();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (ileKlikniecBut3 < 3)
            {
                int ile = sprawdzKlikniete();

                switch (biezace)
                {
                    case 1:
                        {
                            if (gracz1.wymienil == false)
                            {

                                wymienKarty(gracz1);
                                //wyswietlKarty(gracz1);
                            }
                            else MessageBox.Show("WYMIENIONO");
                        }
                        break;
                    case 2:
                        {
                            if (gracz2.wymienil == false)
                            {
                                wymienKarty(gracz2);
                                //wyswietlKarty(gracz2);
                            }
                            else MessageBox.Show("WYMIENIONO");
                        }
                        break;
                }
                pictureBox15.Location = new Point(pictureBox15.Location.X, 346);
                pictureBox14.Location = new Point(pictureBox14.Location.X, 346);
                pictureBox13.Location = new Point(pictureBox13.Location.X, 346);
                pictureBox12.Location = new Point(pictureBox12.Location.X, 346);
                pictureBox11.Location = new Point(pictureBox11.Location.X, 346);
                for (int i = 0; i < 5; i++)
                    klikniete[i] = false;
                ileKliknietych = 0;

                przelaczeniePoWymianie();
            }
            else
            {
                //SĄD
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ileDoPuli += int.Parse(numericUpDown1.Value.ToString());
            label3.Text = "Pula: " + ileDoPuli;
        }
    }
}
